## Steps to follow in order to start video call

## 1) Run the mock API using below commands =>
* npm i //to install dependencies
* npm run dev //to start local API server on your local

## 2) Run the video call repo using below commands => 
* npm start //will start react app on localhost:3000 and server on PORT 8081
* Once you start the app it will automatically take you to localhost:3000 
* To actually use video call you need to generate an auth token first
    * Hit this PUT "https://staging-api.novadonticsllc.com/v4/session" API using postman with {"identifier":"lakshay.gupta@unthinkable.co","secret":"Qwerty12","application":"web"} credentials passed in the body
    * You will obtain a token
* Go back to localhost:3000 and add this route in param => localhost:3000/room/<generated-token>/<any-string/number>

## 3) Steps to start video call access token grant on localhost:8000 =>
* That code resides in the video/call/server branch
* To start the server 1st checkout in that branch and start th server using => npm run dev command
    