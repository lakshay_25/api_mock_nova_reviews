const express = require("express");
const api_helper = require("./API_helper");
const axios = require("axios");
const app = express();

const accountSid = "AC34144ac9dd43ac21851e1b253bef49a7";
const authToken = "97788daa5f5fa7544de20252fc928651";
const client = require("twilio")(accountSid, authToken);

const reviewData = [
  {
    reviewId: "1",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "2",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "3",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "4",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "5",
    userName: "Mathew B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "6",
    userName: "Angelina B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "7",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "8",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "9",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "10",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "11",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "12",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "13",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "14",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
  {
    reviewId: "15",
    userName: "Angela B",
    ratings: 4,
    review:
      "BIO Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
    date: new Date(),
    providerName: "Dr William Whitman",
  },
];

const patientData = [
  {
    id: 1,
    firstName: "Novac",
    lastName: "Merik",
    relationship: "Myself",
    insuranceFront:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/insurancecarddummy.jpg-1650385430179.jpeg",
    insuranceBack:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/insurancecarddummy3.jpeg-1650385411780.jpeg",
  },
  {
    id: 2,
    firstName: "Family",
    lastName: "Member 1",
    relationship: "Uncle",
    insuranceFront: "",
    insuranceBack: "",
  },
  {
    id: 3,
    firstName: "Family",
    lastName: "Member2",
    relationship: "Aunt",
    insuranceFront:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/insurancecarddummy3.jpeg-1650385411780.jpeg",
    insuranceBack:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/insurancecarddummy3.jpeg-1650385411780.jpeg",
  },
];

const availabilityData = [
  {
    providerId: 1,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
        isBlocked: true,
      },
      {
        id: "2",
        startDateTime: "2022-05-28 12:00:00.000",
        endDateTime: "2022-05-28 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
  {
    providerId: 137,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
        isBlocked: true,
      },
      {
        id: "2",
        startDateTime: "2022-05-28 12:00:00.000",
        endDateTime: "2022-05-28 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
  {
    providerId: 2,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
  {
    providerId: 4,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
        isBlocked: true,
      },
      {
        id: "2",
        startDateTime: "2022-05-28 12:00:00.000",
        endDateTime: "2022-05-28 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
  {
    providerId: 131,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
        isBlocked: true,
      },
      {
        id: "2",
        startDateTime: "2022-05-28 12:00:00.000",
        endDateTime: "2022-05-28 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
  {
    providerId: 167,
    slots: [
      {
        id: "1",
        startDateTime: "2022-05-31 10:00:00.000",
        endDateTime: "2022-05-31 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "10:30",
        end: "11:00",
        onlyVirtual: true,
      },
      {
        id: "2",
        startDateTime: "2022-05-28 12:00:00.000",
        endDateTime: "2022-05-28 14:00:00.000",
        from: new Date("2022-05-23"),
        to: new Date("2022-05-23"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 07:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 12:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 14:00:00.000",
        endDateTime: "2022-05-30 15:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 15:00:00.000",
        endDateTime: "2022-05-30 18:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 18:00:00.000",
        endDateTime: "2022-07-30 20:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 09:00:00.000",
        endDateTime: "2022-07-29 10:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 17:00:00.000",
        endDateTime: "2022-07-29 19:00:00.000",
      },
    ],
  },
  {
    providerId: 73,
    slots: [
      {
        id: "3",
        startDateTime: "2022-06-02 06:00:00.000",
        endDateTime: "2022-06-02 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "12:30",
        end: "13:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "4",
        startDateTime: "2022-06-01 06:00:00.000",
        endDateTime: "2022-06-01 13:00:00.000",
        from: new Date("2022-05-24"),
        to: new Date("2022-05-24"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "5",
        startDateTime: "2022-05-25 06:00:00.000",
        endDateTime: "2022-05-25 13:00:00.000",
        from: new Date("2022-05-25"),
        to: new Date("2022-05-25"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "6",
        startDateTime: "2022-05-27 06:00:00.000",
        endDateTime: "2022-05-27 13:00:00.000",
        from: new Date("2022-05-26"),
        to: new Date("2022-05-26"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "7",
        startDateTime: "2022-05-29 06:00:00.000",
        endDateTime: "2022-05-29 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "8",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-27"),
        to: new Date("2022-05-27"),
        start: "13:00",
        end: "13:30",
        onlyVirtual: false,
        isBlocked: false,
      },
      {
        id: "9",
        startDateTime: "2022-05-30 06:00:00.000",
        endDateTime: "2022-05-30 13:00:00.000",
        from: new Date("2022-05-28"),
        to: new Date("2022-05-28"),
        start: "11:30",
        end: "12:00",
        onlyVirtual: true,
        isBlocked: false,
      },
      {
        id: "129",
        providerId: "1",
        onlyVirtual: true,
        date: "2022-07-30",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-30 06:00:00.000",
        endDateTime: "2022-07-30 13:00:00.000",
      },
      {
        id: "204",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "07:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 07:00:00.000",
      },
      {
        id: "203",
        providerId: "1",
        onlyVirtual: false,
        date: "2022-07-29",
        startTime: "06:00:00.000",
        endTime: "13:00:00.000",
        startDateTime: "2022-07-29 06:00:00.000",
        endDateTime: "2022-07-29 13:00:00.000",
      },
    ],
  },
];

const paymentProfiles = {
  profile: {
    paymentProfiles: [
      {
        customerPaymentProfileId: "508013866",
        payment: {
          creditCard: {
            cardNumber: "XXXX1111",
            expirationDate: "2023-12",
            cardType: "Visa",
            issuerNumber: "411111",
          },
        },
        originalNetworkTransId: "0DETH03YLTZ19HO7BPD4XZA",
        originalAuthAmount: 0.0,
        billTo: {
          phoneNumber: "000-000-0000",
          firstName: "John",
          lastName: "Doe",
          address: "123 Main St.",
          city: "Bellevue",
          state: "WA",
          zip: "98004",
          country: "US",
        },
      },
      {
        customerPaymentProfileId: "508013817",
        payment: {
          creditCard: {
            cardNumber: "XXXX1111",
            expirationDate: "2025-12",
            cardType: "Visa",
            issuerNumber: "411111",
          },
        },
        customerType: "individual",
        defaultPaymentProfile: true,
      },
    ],
    profileType: "regular",
    customerProfileId: "505055465",
    merchantCustomerId: "Merchant_Customer_ID",
    description: "Profile description here",
    email: "customer-profile-email@here.com",
  },
  messages: {
    resultCode: "Ok",
    message: [{ code: "I00001", text: "Successful." }],
  },
};

//Used only to deal with CORS origin error
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  next();
});

//ekglist?page=1,limit=10,
app.get("/review", (req, res) => {
  const results = {};
  const page = parseInt(req.query.page);
  const limit = parseInt(req.query.limit);

  const startIndex = (page - 1) * limit;
  const endIndex = page * limit;

  results.results = reviewData.slice(startIndex, endIndex);

  results.totalCount = reviewData.length;

  if (results.results.length !== 0) {
    results.currentPage = page;
    results.limit = limit;
  } else {
    results.currentPage = "";
    results.limit = limit;
  }

  res.json(results);
});

app.get("/patientSelect", (req, res) => {
  res.json(patientData);
});

let roomSid = "";
app.get("/createRoom", async (req, res) => {
  const user_token = req.query.token;
  const bookingId = req.query.bookingId;

  console.log(user_token, bookingId);

  const userName = "Lakshhh";
  const userProfilePic =
    "https://novadontics-staging1-uploads.s3.amazonaws.com/profile%2002.jpg-1651666361651.jpeg";
  const providerProfileImage =
    "https://novadontics-staging1-uploads.s3.amazonaws.com/smartselect_20210712-022516_gallery.jpg-1651828855656.jpeg";
  const providerTitle = "Dr.";
  const providerFirstName = "Lakshay";
  const providerLastName = "Gupta";
  const role = "General Dentist";

  if (roomSid) {
    console.log(roomSid);
    try {
      await client.video
        .rooms(roomSid)
        // .rooms(roomSid)
        .fetch()
        .then((room) => {
          room["userName"] = userName;
          room["userProfilePic"] = userProfilePic;
          room["providerProfileImage"] = providerProfileImage;
          room["providerTitle"] = providerTitle;
          room["providerFirstName"] = providerFirstName;
          room["providerLastName"] = providerLastName;
          room["role"] = role;
          res.json(room);
        })
        .catch((err) => {
          console.log(err);
          res.send(err);
        });
    } catch (err) {
      console.log(err);
    }
    return;
  }
  try {
    await client.video.rooms
      .create({
        uniqueName: "Daily Standup 111",
        emptyRoomTimeout: "15", //time till which room remains active after the last participant leaves.
        unusedRoomTimeout: "15", //the amount of time room remains active if no one joins.
        maxParticipantDuration: "1800",
      })
      .then((room) => {
        room["userName"] = userName;
        room["userProfilePic"] = userProfilePic;
        room["providerProfileImage"] = providerProfileImage;
        room["providerTitle"] = providerTitle;
        room["providerFirstName"] = providerFirstName;
        room["providerLastName"] = providerLastName;
        room["role"] = role;
        roomSid = room["sid"];
        res.json(room);
      })
      .catch((err) => {
        console.log(err);
        res.send(err);
      });
  } catch (err) {
    console.log(err);
  }
});

app.get("/hardCodedRoom", async (req, res) => {
  const obj = {
    appointmentId: "1386",
    isProvider: false,
    patientId: "3186",
    patientName: "Lakshay Gupta",
    patientProfileImage:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/smartselect_20210712-022516_gallery.jpg-1656255648537.jpeg",
    practiceId: "1",
    providerFirstName: "Albie",
    providerId: "1",
    providerLastName: "Morkel",
    providerProfileImage:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/dataurifile1649146118258-1649146118893.png",
    providerTitle: "Mr.",
    role: "General Dentist",
    roomName: "videoRoom:1386",
    roomSid: "RM7cc37d81bfe439dac86fc6f407b313e2",
  };
  res.json(obj);
});

//http://localhost:5000/getAllRecordings?roomSid=RMef72cc0f4444c28047fe0ea81cc5c6b0
app.get("/getAllRecordings", async (req, res) => {
  const roomSid = req.query.roomSid;
  try {
    await client.video.recordings
      .list({
        groupingSid: [roomSid],
        limit: 20,
      })
      .then((recordings) => res.json(recordings));
    // .then((recordings) => recordings.forEach((r) => console.log(r.sid)));
  } catch (err) {
    console.log(err);
  }
});

app.get("/composition", async (req, res) => {
  const roomSid = req.query.roomSid;
  const audioRecording = req.query.audioRecording;
  const videoRecording = req.query.videoRecording;
  client.video.compositions
    .create({
      roomSid: roomSid,
      audioSources: audioRecording,
      videoLayout: {
        sequence: {
          max_rows: 1,
          max_columns: 1,
          reuse: "show_newest",
          video_sources: [videoRecording],
        },
      },
      statusCallback: "http://my.server.org/callbacks",
      format: "mp4",
      resolution: "1280x720",
    })
    .then((composition) => {
      res.json(composition);
    });
});

app.get("/completeRoomComposition", async (req, res) => {
  const roomSid = req.query.roomSid;
  client.video.compositions
    .create({
      roomSid: roomSid,
      audioSources: "*",
      videoLayout: {
        grid: {
          video_sources: ["*"],
        },
      },
      statusCallback: "http://my.server.org/callbacks",
      format: "mp4",
    })
    .then((composition) => {
      res.json(composition);
    });
});

app.get("/providerAvailability", (req, res) => {
  const providerId = req.query.providerId;
  const availableSlotsOfProvider = availabilityData.find(
    (slotData) => slotData.providerId === +providerId
  );
  res.json(availableSlotsOfProvider);
});

app.post("/addSlots", (req, res) => {
  // const providerId = req.body.providerId;
  const newSlot = req.body.newSlot;
  for (let i = 0; i < availabilityData.length; i++) {
    if (availabilityData[i].providerId === 1) {
      availabilityData[i].slots.unshift(newSlot);
    }
  }
  return res.status(200).json({
    message: "Added successfully",
  });
});

//localhost:5000/getANetCreentials
app.get("/getANetCreentials", (req, res) => {
  const credentials = {
    apiLoginID: "8SehxU7657",
    clientPublicKey:
      "8jVwsQyaJhsby38hEVYyGuR8Tf2y2AL3cyyj9qhHkXjQjwbZg2y45s2N9U55YK25",
    apiUrl: "https://apitest.authorize.net/xml/v1/request.api",
    validationMode: "testMode",
  };
  res.json(credentials);
});

//localhost:5000/getPaymentProfile
app.get("/getPaymentProfile", async (req, res) => {
  res.json(paymentProfiles);
});

app.get("/getHardcodedRoom", (req, res) => {
  const obj = {
    sid: "RM6578ef44d4c37bb25d4979430fa8e6e6",
    status: "in-progress",
    dateCreated: "2022-08-05T08:21:55.000Z",
    dateUpdated: "2022-08-05T08:21:55.000Z",
    accountSid: "AC34144ac9dd43ac21851e1b253bef49a7",
    enableTurn: true,
    uniqueName: "Daily Standup 2",
    statusCallback: null,
    statusCallbackMethod: "POST",
    endTime: null,
    duration: null,
    type: "group",
    maxParticipants: 50,
    maxParticipantDuration: 1800,
    maxConcurrentPublishedTracks: 170,
    recordParticipantsOnConnect: false,
    videoCodecs: ["H264", "VP8"],
    mediaRegion: "us1",
    audioOnly: false,
    emptyRoomTimeout: 15,
    unusedRoomTimeout: 15,
    url: "https://video.twilio.com/v1/Rooms/RM6578ef44d4c37bb25d4979430fa8e6e6",
    links: {
      recordings:
        "https://video.twilio.com/v1/Rooms/RM6578ef44d4c37bb25d4979430fa8e6e6/Recordings",
      participants:
        "https://video.twilio.com/v1/Rooms/RM6578ef44d4c37bb25d4979430fa8e6e6/Participants",
      recording_rules:
        "https://video.twilio.com/v1/Rooms/RM6578ef44d4c37bb25d4979430fa8e6e6/RecordingRules",
    },
    userName: "Lakshay",
    userProfilePic:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/profile%2002.jpg-1651666361651.jpeg",
    providerProfileImage:
      "https://novadontics-staging1-uploads.s3.amazonaws.com/smartselect_20210712-022516_gallery.jpg-1651828855656.jpeg",
    providerTitle: "Dr.",
    providerFirstName: "Lakshay",
    providerLastName: "Gupta",
    role: "General Dentist",
  };
  res.json(obj);
});

app.listen(5001);
